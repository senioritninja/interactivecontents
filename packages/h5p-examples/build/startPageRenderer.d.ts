import * as H5P from '../../h5p-server';
export default function render(editor: H5P.H5PEditor): (req: any, res: any) => any;
