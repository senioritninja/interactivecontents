import * as React from 'react';
import { Component, createRef } from 'react';
import { defineElements } from '@lumieducation/h5p-webcomponents';
defineElements('h5p-editor');
export default class H5PEditorUI extends Component {
    constructor(props) {
        super(props);
        this.loadContentCallbackWrapper = (contentId) => {
            return this.props.loadContentCallback(contentId);
        };
        this.onEditorLoaded = (event) => {
            if (this.props.onLoaded) {
                this.props.onLoaded(event.detail.contentId, event.detail.ubername);
            }
        };
        this.onSaved = (event) => {
            if (this.props.onSaved) {
                this.props.onSaved(event.detail.contentId, event.detail.metadata);
            }
        };
        this.onSaveError = async (event) => {
            if (this.props.onSaveError) {
                this.props.onSaveError(event.detail.message);
            }
        };
        this.saveContentCallbackWrapper = (contentId, requestBody) => {
            return this.props.saveContentCallback(contentId, requestBody);
        };
        this.h5pEditor = createRef();
    }
    componentDidMount() {
        this.registerEvents();
        this.setServiceCallbacks();
    }
    componentDidUpdate() {
        var _a;
        this.registerEvents();
        this.setServiceCallbacks();
        (_a = this.h5pEditor.current) === null || _a === void 0 ? void 0 : _a.resize();
    }
    componentWillUnmount() {
        this.unregisterEvents();
    }
    getSnapshotBeforeUpdate() {
        // Should the old editor instance be destroyed, we unregister from it...
        this.unregisterEvents();
        return null;
    }
    render() {
        return (React.createElement("h5p-editor", { ref: this.h5pEditor, "content-id": this.props.contentId }));
    }
    /**
     * Call this method to save the current state of the h5p editor. This will
     * result in a call to the `saveContentCallback` that was passed in the
     * through the props.
     * @throws an error if there was a problem (e.g. validation error of the
     * content)
     */
    async save() {
        var _a;
        try {
            return await ((_a = this.h5pEditor.current) === null || _a === void 0 ? void 0 : _a.save());
        }
        catch (error) {
            // We ignore the error, as we subscribe to the 'save-error' and
            // 'validation-error' events.
        }
    }
    registerEvents() {
        var _a, _b, _c, _d;
        (_a = this.h5pEditor.current) === null || _a === void 0 ? void 0 : _a.addEventListener('saved', this.onSaved);
        (_b = this.h5pEditor.current) === null || _b === void 0 ? void 0 : _b.addEventListener('editorloaded', this.onEditorLoaded);
        (_c = this.h5pEditor.current) === null || _c === void 0 ? void 0 : _c.addEventListener('save-error', this.onSaveError);
        (_d = this.h5pEditor.current) === null || _d === void 0 ? void 0 : _d.addEventListener('validation-error', this.onSaveError);
    }
    setServiceCallbacks() {
        if (this.h5pEditor.current) {
            this.h5pEditor.current.loadContentCallback =
                this.loadContentCallbackWrapper;
            this.h5pEditor.current.saveContentCallback =
                this.saveContentCallbackWrapper;
        }
    }
    unregisterEvents() {
        var _a, _b, _c, _d;
        (_a = this.h5pEditor.current) === null || _a === void 0 ? void 0 : _a.removeEventListener('saved', this.onSaved);
        (_b = this.h5pEditor.current) === null || _b === void 0 ? void 0 : _b.removeEventListener('editorloaded', this.onEditorLoaded);
        (_c = this.h5pEditor.current) === null || _c === void 0 ? void 0 : _c.removeEventListener('save-error', this.onSaveError);
        (_d = this.h5pEditor.current) === null || _d === void 0 ? void 0 : _d.removeEventListener('validation-error', this.onSaveError);
    }
}
//# sourceMappingURL=H5PEditorUI.js.map