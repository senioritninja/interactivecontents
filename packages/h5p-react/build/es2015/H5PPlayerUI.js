import * as React from 'react';
import { Component, createRef } from 'react';
import { defineElements } from '@lumieducation/h5p-webcomponents';
defineElements('h5p-player');
export default class H5PPlayerUI extends Component {
    constructor(props) {
        super(props);
        this.loadContentCallbackWrapper = (contentId) => this.props.loadContentCallback(contentId);
        this.onInitialized = (event) => {
            if (this.props.onInitialized) {
                this.props.onInitialized(event.detail.contentId);
            }
        };
        this.onxAPIStatement = (event) => {
            if (this.props.onxAPIStatement) {
                this.props.onxAPIStatement(event.detail.statement, event.detail.context, event.detail.event);
            }
        };
        this.h5pPlayer = createRef();
    }
    componentDidMount() {
        this.registerEvents();
        this.setServiceCallbacks();
    }
    componentDidUpdate() {
        this.registerEvents();
        this.setServiceCallbacks();
    }
    componentWillUnmount() {
        this.unregisterEvents();
    }
    /**
     * The internal H5P instance object of the H5P content.
     *
     * Only available after the `initialized` event was fired. Important: This
     * object is only partially typed and there are more properties and methods
     * on it!
     */
    get h5pInstance() {
        var _a;
        return (_a = this.h5pPlayer.current) === null || _a === void 0 ? void 0 : _a.h5pInstance;
    }
    /**
     * The global H5P object / namespace (normally accessible through "H5P..."
     * or "window.H5P") of the content type. Depending on the embed type this
     * can be an object from the internal iframe, so you can use it to break the
     * barrier of the iframe and execute JavaScript inside the iframe.
     *
     * Only available after the `initialized` event was fired. Important: This
     * object is only partially typed and there are more properties and methods
     * on it!
     */
    get h5pObject() {
        var _a;
        return (_a = this.h5pPlayer.current) === null || _a === void 0 ? void 0 : _a.h5pObject;
    }
    /**
     * Returns the copyright notice in HTML that you can insert somewhere to
     * display it. Undefined if there is no copyright information.
     */
    getCopyrightHtml() {
        var _a, _b;
        return (_b = (_a = this.h5pPlayer.current) === null || _a === void 0 ? void 0 : _a.getCopyrightHtml()) !== null && _b !== void 0 ? _b : '';
    }
    getSnapshotBeforeUpdate() {
        // Should the old editor instance be destroyed, we unregister from it...
        this.unregisterEvents();
        return null;
    }
    /**
     * @returns true if there is copyright information to be displayed.
     */
    hasCopyrightInformation() {
        var _a;
        return (_a = this.h5pPlayer.current) === null || _a === void 0 ? void 0 : _a.hasCopyrightInformation();
    }
    render() {
        return (React.createElement("h5p-player", { ref: this.h5pPlayer, "content-id": this.props.contentId }));
    }
    /**
     * Displays the copyright notice in the regular H5P way.
     */
    showCopyright() {
        var _a;
        (_a = this.h5pPlayer.current) === null || _a === void 0 ? void 0 : _a.showCopyright();
    }
    /**
     * Asks the H5P content to resize itself inside the dimensions of the
     * container.
     *
     * Has no effect until the H5P object has fully initialized.
     */
    resize() {
        var _a;
        (_a = this.h5pPlayer.current) === null || _a === void 0 ? void 0 : _a.resize();
    }
    registerEvents() {
        var _a, _b;
        (_a = this.h5pPlayer.current) === null || _a === void 0 ? void 0 : _a.addEventListener('initialized', this.onInitialized);
        if (this.props.onxAPIStatement) {
            (_b = this.h5pPlayer.current) === null || _b === void 0 ? void 0 : _b.addEventListener('xAPI', this.onxAPIStatement);
        }
    }
    setServiceCallbacks() {
        if (this.h5pPlayer.current) {
            this.h5pPlayer.current.loadContentCallback =
                this.loadContentCallbackWrapper;
        }
    }
    unregisterEvents() {
        var _a, _b;
        (_a = this.h5pPlayer.current) === null || _a === void 0 ? void 0 : _a.removeEventListener('initialized', this.onInitialized);
        if (this.props.onxAPIStatement) {
            (_b = this.h5pPlayer.current) === null || _b === void 0 ? void 0 : _b.removeEventListener('xAPI', this.onxAPIStatement);
        }
    }
}
//# sourceMappingURL=H5PPlayerUI.js.map