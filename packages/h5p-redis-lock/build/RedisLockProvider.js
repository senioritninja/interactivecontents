"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var simple_redis_mutex_1 = require("simple-redis-mutex");
var h5p_server_1 = require("@lumieducation/h5p-server");
var log = new h5p_server_1.Logger('RedisLockProvider');
var RedisLockProvider = /** @class */ (function () {
    function RedisLockProvider(redis, options) {
        this.redis = redis;
        this.options = options;
        log.debug('initialize');
    }
    RedisLockProvider.prototype.acquire = function (key, callback, options) {
        var _a, _b;
        return __awaiter(this, void 0, void 0, function () {
            var unlock, error_1, timeout_1, cancelPromise_1, timeoutPromise, result;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _c.trys.push([0, 2, , 3]);
                        log.debug("Attempting to acquire lock for key ".concat(key, "."));
                        return [4 /*yield*/, (0, simple_redis_mutex_1.lock)(this.redis, key, {
                                timeoutMillis: options.maxOccupationTime,
                                failAfterMillis: options.timeout,
                                retryTimeMillis: (_b = (_a = this.options) === null || _a === void 0 ? void 0 : _a.retryTime) !== null && _b !== void 0 ? _b : 5
                            })];
                    case 1:
                        unlock = _c.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _c.sent();
                        if (error_1.message.startsWith('Lock could not be acquire for')) {
                            // the spelling mistake was made in the library...
                            log.debug("There was a timeout when trying to acquire key for ".concat(key));
                            throw new Error('timeout');
                        }
                        return [3 /*break*/, 3];
                    case 3:
                        _c.trys.push([3, , 5, 8]);
                        timeoutPromise = new Promise(function (res, rej) {
                            cancelPromise_1 = rej;
                            timeout_1 = setTimeout(function () {
                                res('occupation-time-exceeded');
                            }, options.maxOccupationTime);
                        });
                        log.debug("Acquired lock for key ".concat(key, ". Calling operation."));
                        return [4 /*yield*/, Promise.race([timeoutPromise, callback()])];
                    case 4:
                        result = _c.sent();
                        if (typeof result === 'string' &&
                            result === 'occupation-time-exceeded') {
                            log.debug("The operation holding the lock for key ".concat(key, " took longer than allowed. Lock was released by Redis."));
                            throw new Error('occupation-time-exceeded');
                        }
                        log.debug("Operation for lock key ".concat(key, " has finished."));
                        clearTimeout(timeout_1);
                        cancelPromise_1();
                        return [2 /*return*/, result];
                    case 5:
                        log.debug("Releasing lock for key ".concat(key));
                        if (!unlock) return [3 /*break*/, 7];
                        return [4 /*yield*/, unlock()];
                    case 6:
                        _c.sent();
                        _c.label = 7;
                    case 7: return [7 /*endfinally*/];
                    case 8: return [2 /*return*/];
                }
            });
        });
    };
    return RedisLockProvider;
}());
exports["default"] = RedisLockProvider;
//# sourceMappingURL=RedisLockProvider.js.map