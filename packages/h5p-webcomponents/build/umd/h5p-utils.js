(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "deepmerge"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.removeUnusedContent = exports.mergeH5PIntegration = void 0;
    const merge = require("deepmerge");
    /**
     * Merges the new IIntegration object with the global one.
     * @param newIntegration
     * @param contentId
     */
    function mergeH5PIntegration(newIntegration, contentId) {
        if (!window.H5PIntegration) {
            window.H5PIntegration = newIntegration;
            return;
        }
        if (contentId &&
            newIntegration.contents &&
            newIntegration.contents[`cid-${contentId}`]) {
            if (!window.H5PIntegration.contents) {
                window.H5PIntegration.contents = {};
            }
            window.H5PIntegration.contents[`cid-${contentId}`] =
                newIntegration.contents[`cid-${contentId}`];
        }
        // We don't want to mutate the newIntegration parameter, so we shallow clone
        // it.
        const newIntegrationDup = Object.assign({}, newIntegration);
        // We don't merge content object information, as there might be issues with
        // this.
        delete newIntegrationDup.contents;
        window.H5PIntegration = merge(window.H5PIntegration, newIntegrationDup);
    }
    exports.mergeH5PIntegration = mergeH5PIntegration;
    /**
     * Removes the data about the content from the global H5PIntegration object.
     * @param contentId
     */
    function removeUnusedContent(contentId) {
        var _a;
        if (((_a = window.H5PIntegration) === null || _a === void 0 ? void 0 : _a.contents) &&
            window.H5PIntegration.contents[`cid-${contentId}`]) {
            delete window.H5PIntegration.contents[`cid-${contentId}`];
        }
    }
    exports.removeUnusedContent = removeUnusedContent;
});
//# sourceMappingURL=h5p-utils.js.map